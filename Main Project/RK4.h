#ifndef RK4_H
#define RK4_H
#include <vector>
using namespace std;

class RK4
{
private:
	vector<double> _x, _x_squared, _X, _dX, _En, _Err;
	double _Q, _iteration, _tau, _max_iterations;
	double _k1, _k2, _k3, _k4, _l1, _l2, _l3, _l4;
	double _Err_total, _Relative_error_mean, _x_squared_total;
	vector<double> _U1, _U2, _Z0; //random numbers for brownian motion method

	//driving
	double _eta, _w0, _wd, _delta, _driving_amplitude, _driving_force;
	double _steady_state_solution, _transient_solution, _full_solution, _phase, _C_1, _C_2, _drive;

	//driving error
	double _err_drive_total, _x_drive_squared_total, _rel_err_drive;
	vector<double> _XSIM, _XANA;
	vector<double> _err_drive, _x_drive_squared;

	//brownian motion standard deviation
	double _standard_deviation, _mean;
	vector<double> _sum_for_sd;

	//probability of bifurcation
	vector<int> _bifurcation, _no_bifurcation;
	double _total_runs, _probability;

public:
	double tau(double dt, double n);
	double f(double dX);
	double g(double X, double dX, double Q);
	double g(double X, double dX, double Q, double Z0); //overloaded for noise
	double g(double X, double dX, double Q, double dt, double n, double addtional_factor, double natural_frequency, double driving_frequency, double eta);
	double g(double X, double dX, double Q, double Z0, double dt, double n, double addtional_factor, double natural_frequency, double driving_frequency, double eta);
	//g for non-linear driven case with noise
	double g_NL(double X, double dX, double Q, double Z0, double dt, double n, double addtional_factor, double natural_frequency, double driving_frequency, double eta);

	void set_max_iterations(int);
	
	void simulate_oscillations(double Q, double dt);
	void simulate_energy(double Q, double dt);
	
	void simulate_brownian_motion(double Q, double dt);
	void calculate_X_standard_deviation(double Q, double dt);
	void calculate_Noise_standard_deviation(double Q, double dt);

	void calculate_error(double Q, double dt, double start_value, bool save_alternate_file);

	void analytical_driving(double Q, double dt, double natural_frequency, double driving_frequency, double eta);

	void simulate_driving(double Q, double dt, double natural_frequency, double driving_frequency, double eta);

	void driving_data_for_Bode_Plot(double Q);

	void calculate_driving_error(double Q, double dt, double natural_frequency, double driving_frequency, double eta);

	void simulate_driving_with_noise(double Q, double dt, double natural_frequency, double driving_frequency, double eta);

	void simulate_NL_driving_with_noise(double Q, double dt, double natural_frequency, double driving_frequency, double eta);

	void detect_bifurcation(double Q, double dt, double natural_frequency, double driving_frequency, double eta);//used to find region of interest

	void calculate_probability_of_bifurcation(int max_runs, double Q, double dt, double natural_frequency, double driving_frequency, double eta);
};

#endif