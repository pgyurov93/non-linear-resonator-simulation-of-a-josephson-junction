#include <iostream>
#include <cmath>
#include <vector>
#include <stdio.h>
#include <sstream>
#include <fstream>
#include <numeric> 


using namespace std;
double tau0 = 0;
double PI = 3.14159265359;

//Comparison & Energy
vector<double> X(0);
vector<double> dX(0);
vector<double> x(0);
vector<double> x_squared(0);
vector<double> XEuler(0);
vector<double> dXEuler(0);
vector<double> xEuler(0);

//Error
vector<double> ErrEuler;
vector<double> Err;



double tau(double n, double deltatau) //This method gives the time location of the system
{
	return tau0 + deltatau*n; //creating a constant time
}
double f(double tau, double X, double dX) //This method returns the derivative of X, unsimplified due to possible evolution.
{
	return dX;
}
double g(double tau, double X, double dX, double Q) //This method returns the quantity associated with the force, unsimplified due to possible evolution.
{
	return -(dX / Q + X);
}

void clearcontents(double Q, double start_value)
{
	stringstream ss;
	ss << "C:/Users/Petar/Documents/My Projects/Simulation of Non-Linear Resonator/3RD YEAR PROJECT/Output/RK4_DATA.txt";
	ofstream oscillatorRK4file;
	oscillatorRK4file.open(ss.str().c_str(), fstream::trunc);
	oscillatorRK4file.close();

	//stringstream ss;
	ss << "C:/Users/Petar/Documents/My Projects/Simulation of Non-Linear Resonator/3RD YEAR PROJECT/Output/EULER_DATA.txt";
	ofstream oscillatorEULERfile;
	oscillatorEULERfile.open(ss.str().c_str(), fstream::trunc);
	oscillatorEULERfile.close();

	for (Q = Q; Q <= 10000; Q = Q * 10)
	{
		stringstream em_E;
		em_E << "C:/Users/Petar/Documents/My Projects/Simulation of Non-Linear Resonator/3RD YEAR PROJECT/Output/Errors/EULER_ERRORMEAN_Q" << double(Q) << ".txt";
		ofstream EULER_ERRORMEANfile;
		EULER_ERRORMEANfile.open(em_E.str().c_str(), fstream::trunc);
		EULER_ERRORMEANfile.close();

		stringstream em_R;
		em_R << "C:/Users/Petar/Documents/My Projects/Simulation of Non-Linear Resonator/3RD YEAR PROJECT/Output/Errors/RK4_ERRORMEAN_Q" << double(Q) << ".txt";
		ofstream RK4_ERRORMEANfile;
		RK4_ERRORMEANfile.open(em_R.str().c_str(), fstream::trunc);
		RK4_ERRORMEANfile.close();

		//stringstream ss;
		ss << "C:/Users/Petar/Documents/My Projects/Simulation of Non-Linear Resonator/3RD YEAR PROJECT/Output/ENERGY_RK4_DATA_Q" << double(Q) << ".txt";
		ofstream ENERGY_RK4file;
		ENERGY_RK4file.open(ss.str().c_str(), fstream::trunc);
		ENERGY_RK4file.close();
	}

	for (start_value = 1; start_value <= 4; start_value++)
	{
		stringstream em_R;
		em_R << "C:/Users/Petar/Documents/My Projects/Simulation of Non-Linear Resonator/3RD YEAR PROJECT/Output/Errors/RK4_ERRORMEAN_Q" << double(30) << "_X" << start_value << ".txt";
		ofstream RK4_ERRORMEANfile;
		RK4_ERRORMEANfile.open(em_R.str().c_str(), ofstream::trunc);
		RK4_ERRORMEANfile.close();
	}
	
}

void RK4(double Q, double deltatau)
{
	x.clear(); //testing if clearing the vectors will fix Q automation
	X.clear();
	dX.clear();
	x.push_back(1);		//Starting value for ANALYTICAL
	X.push_back(1);     //Starting value for SIMULATION
	dX.push_back(0);    //Starting value for dX	

	stringstream ss;
	ss << "C:/Users/Petar/Documents/My Projects/Simulation of Non-Linear Resonator/3RD YEAR PROJECT/Output/RK4_DATA.txt";
	ofstream oscillatorRK4file;
	oscillatorRK4file.open(ss.str().c_str());

	double k1, l1, k2, l2, k3, l3, k4, l4;
	for (int n = 0; n <= 100000; ++n)
	{
		k1 = deltatau*f(tau(n, deltatau), X.at(n), dX.at(n));
		l1 = deltatau*g(tau(n, deltatau), X.at(n), dX.at(n), Q);
		k2 = deltatau*f((tau(n, deltatau) + 0.5*deltatau), (X.at(n) + 0.5*k1), (dX.at(n) + 0.5*l1));
		l2 = deltatau*g((tau(n, deltatau) + 0.5*deltatau), (X.at(n) + 0.5*k1), (dX.at(n) + 0.5*l1), Q);
		k3 = deltatau*f((tau(n, deltatau) + 0.5*deltatau), (X.at(n) + 0.5*k2), (dX.at(n) + 0.5*l2));
		l3 = deltatau*g((tau(n, deltatau) + 0.5*deltatau), (X.at(n) + 0.5*k2), (dX.at(n) + 0.5*l2), Q);
		k4 = deltatau*f((tau(n, deltatau) + deltatau), (X.at(n) + k3), (dX.at(n) + l3));
		l4 = deltatau*g((tau(n, deltatau) + deltatau), (X.at(n) + k3), (dX.at(n) + l3), Q);
		X.push_back((X.at(n) + (k1 / 6) + (k2 / 3) + (k3 / 3) + (k4 / 6)));
		dX.push_back((dX.at(n) + (l1 / 6) + (l2 / 3) + (l3 / 3) + (l4 / 6)));
		x.push_back(exp(-(tau(n, deltatau)) / (2 * Q)) * (cos(tau(n, deltatau) * (sqrt(1 - (1 / (4 * pow(Q, 2))))))));

		oscillatorRK4file << n << "\t" << X.at(n) << "\t" << dX.at(n) << "\t" << x.at(n) << endl;

	}
	oscillatorRK4file.close();
}

void Euler(double Q, double deltatau)
{
	xEuler.clear();
	XEuler.clear();
	dXEuler.clear();

	xEuler.push_back(1);	 //Starting value for ANALYTICAL
	XEuler.push_back(1);     //Starting value for SIMULATION
	dXEuler.push_back(0);    //Starting value for dX

	stringstream ss;
	ss << "C:/Users/Petar/Documents/My Projects/Simulation of Non-Linear Resonator/3RD YEAR PROJECT/Output/EULER_DATA.txt";
	ofstream oscillatorEULERfile;
	oscillatorEULERfile.open(ss.str().c_str());

	for (int n = 0; n <= 100000; ++n)
	{
		XEuler.push_back(XEuler.at(n) + dXEuler.at(n)*deltatau);
		dXEuler.push_back((-dXEuler.at(n) / Q - XEuler.at(n))*deltatau + dXEuler.at(n));
		xEuler.push_back(exp(-(tau(n, deltatau)) / (2 * Q)) * (cos(tau(n, deltatau) * (sqrt(1 - (1 / (4 * pow(Q, 2))))))));
		ErrEuler.push_back(abs(xEuler.at(n) - XEuler.at(n)));
		oscillatorEULERfile << n << "\t" << XEuler.at(n) << "\t" << dXEuler.at(n) << "\t" << xEuler.at(n) << endl;
	}
	oscillatorEULERfile.close();
}

void Error(double Q, double start_value, double deltatau, bool save_alternate_file) //start_value is to specify the starting value of X
{
	x.clear(); //testing if clearing the vectors will fix Q automation
	x_squared.clear();
	X.clear();
	dX.clear();

	x.push_back(start_value);
	X.push_back(start_value);
	dX.push_back(0);

	xEuler.clear();
	XEuler.clear();
	dXEuler.clear();

	xEuler.push_back(1);
	XEuler.push_back(1);
	dXEuler.push_back(0);

	Err.clear();
	ErrEuler.clear();
	//Err.push_back(0);
	//ErrEuler.push_back(0);

	if (save_alternate_file == false)
	{
		stringstream em_E;
		em_E << "C:/Users/Petar/Documents/My Projects/Simulation of Non-Linear Resonator/3RD YEAR PROJECT/Output/Errors/EULER_ERRORMEAN_Q" << double(Q) << ".txt";
		ofstream EULER_ERRORMEANfile;
		EULER_ERRORMEANfile.open(em_E.str().c_str(), ofstream::app);

		stringstream em_R;
		em_R << "C:/Users/Petar/Documents/My Projects/Simulation of Non-Linear Resonator/3RD YEAR PROJECT/Output/Errors/RK4_ERRORMEAN_Q" << double(Q) << ".txt";
		ofstream RK4_ERRORMEANfile;
		RK4_ERRORMEANfile.open(em_R.str().c_str(), ofstream::app);

		double errEuler, errtotEuler, Rel_errmeanEuler;
		double k1, l1, k2, l2, k3, l3, k4, l4, err, errtot, Rel_errmean, x_squared_tot;

		for (int n = 0; n <= 10000; ++n)
		{
			k1 = deltatau*f(tau(n, deltatau), X.at(n), dX.at(n));
			l1 = deltatau*g(tau(n, deltatau), X.at(n), dX.at(n), Q);
			k2 = deltatau*f((tau(n, deltatau) + 0.5*deltatau), (X.at(n) + 0.5*k1), (dX.at(n) + 0.5*l1));
			l2 = deltatau*g((tau(n, deltatau) + 0.5*deltatau), (X.at(n) + 0.5*k1), (dX.at(n) + 0.5*l1), Q);
			k3 = deltatau*f((tau(n, deltatau) + 0.5*deltatau), (X.at(n) + 0.5*k2), (dX.at(n) + 0.5*l2));
			l3 = deltatau*g((tau(n, deltatau) + 0.5*deltatau), (X.at(n) + 0.5*k2), (dX.at(n) + 0.5*l2), Q);
			k4 = deltatau*f((tau(n, deltatau) + deltatau), (X.at(n) + k3), (dX.at(n) + l3));
			l4 = deltatau*g((tau(n, deltatau) + deltatau), (X.at(n) + k3), (dX.at(n) + l3), Q);
			X.push_back((X.at(n) + (k1 / 6) + (k2 / 3) + (k3 / 3) + (k4 / 6)));       //Formulation of the incremental value of X
			dX.push_back((dX.at(n) + (l1 / 6) + (l2 / 3) + (l3 / 3) + (l4 / 6)));     //Formulation of the incremental value of dX
			x.push_back(exp(-(tau(n, deltatau)) / (2 * Q)) * (cos(tau(n, deltatau) * (sqrt(1 - (1 / (4 * pow(Q, 2))))))));
			x_squared.push_back(pow(x.at(n), 2));
			Err.push_back(pow(x.at(n) - X.at(n), 2));
			if (x.at(0) == 0)
				cout << "sdgsd" << endl;
			XEuler.push_back(XEuler.at(n) + dXEuler.at(n)*deltatau);
			dXEuler.push_back((-dXEuler.at(n) / Q - XEuler.at(n))*deltatau + dXEuler.at(n));
			xEuler.push_back(exp(-(tau(n, deltatau)) / (2 * Q)) * (cos(tau(n, deltatau) * (sqrt(1 - (1 / (4 * pow(Q, 2))))))));
			ErrEuler.push_back(pow(xEuler.at(n) - XEuler.at(n), 2));

			if (n == 10000)
			{
				errtot = accumulate(Err.begin(), Err.end(), double(0));
				x_squared_tot = accumulate(x_squared.begin(), x_squared.end(), double(0));
				Rel_errmean = (errtot / x_squared_tot) / n;

				RK4_ERRORMEANfile << Rel_errmean << "\t" << deltatau << endl;

				errtotEuler = accumulate(ErrEuler.begin(), ErrEuler.end(), double(0));
				Rel_errmeanEuler = (errtotEuler / x_squared_tot) / n;
				EULER_ERRORMEANfile << Rel_errmeanEuler << "\t" << deltatau << endl;
			}
		}
		RK4_ERRORMEANfile.close();
		EULER_ERRORMEANfile.close();
	}
	else
	{
		stringstream em_R;
		em_R << "C:/Users/Petar/Documents/My Projects/Simulation of Non-Linear Resonator/3RD YEAR PROJECT/Output/Errors/RK4_ERRORMEAN_Q" << double(Q) << "_X" << double(start_value) << ".txt";
		ofstream RK4_ERRORMEANfile;
		RK4_ERRORMEANfile.open(em_R.str().c_str(), ofstream::app);

		double k1, l1, k2, l2, k3, l3, k4, l4, err, errtot, Rel_errmean, x_squared_tot;

		for (int n = 0; n <= 10000; ++n)
		{
			k1 = deltatau*f(tau(n, deltatau), X.at(n), dX.at(n));
			l1 = deltatau*g(tau(n, deltatau), X.at(n), dX.at(n), Q);
			k2 = deltatau*f((tau(n, deltatau) + 0.5*deltatau), (X.at(n) + 0.5*k1), (dX.at(n) + 0.5*l1));
			l2 = deltatau*g((tau(n, deltatau) + 0.5*deltatau), (X.at(n) + 0.5*k1), (dX.at(n) + 0.5*l1), Q);
			k3 = deltatau*f((tau(n, deltatau) + 0.5*deltatau), (X.at(n) + 0.5*k2), (dX.at(n) + 0.5*l2));
			l3 = deltatau*g((tau(n, deltatau) + 0.5*deltatau), (X.at(n) + 0.5*k2), (dX.at(n) + 0.5*l2), Q);
			k4 = deltatau*f((tau(n, deltatau) + deltatau), (X.at(n) + k3), (dX.at(n) + l3));
			l4 = deltatau*g((tau(n, deltatau) + deltatau), (X.at(n) + k3), (dX.at(n) + l3), Q);
			X.push_back((X.at(n) + (k1 / 6) + (k2 / 3) + (k3 / 3) + (k4 / 6)));       //Formulation of the incremental value of X
			dX.push_back((dX.at(n) + (l1 / 6) + (l2 / 3) + (l3 / 3) + (l4 / 6)));     //Formulation of the incremental value of dX
			x.push_back(exp(-(tau(n, deltatau)) / (2 * Q)) * (cos(tau(n, deltatau) * (sqrt(1 - (1 / (4 * pow(Q, 2))))))));
			x_squared.push_back(pow(x.at(n), 2));
			Err.push_back(pow(x.at(n) - X.at(n), 2));

			if (n == 10000)
			{
				errtot = accumulate(Err.begin(), Err.end(), double(0)); //change label
				x_squared_tot = accumulate(x_squared.begin(), x_squared.end(), double(0));
				Rel_errmean = (errtot / x_squared_tot) / n;
				RK4_ERRORMEANfile << Rel_errmean << "\t" << deltatau << endl;
			}
		}
		RK4_ERRORMEANfile.close();
	}
}

void Energy(double Q, double deltatau)
{
	x.clear(); //testing if clearing the vectors will fix Q automation
	X.clear();
	dX.clear();
	x.push_back(1);		//Starting value for ANALYTICAL
	X.push_back(1);     //Starting value for SIMULATION
	dX.push_back(0);    //Starting value for dX	

	stringstream ss;
	ss << "C:/Users/Petar/Documents/My Projects/Simulation of Non-Linear Resonator/3RD YEAR PROJECT/Output/ENERGY_RK4_DATA_Q" << double(Q) << ".txt";
	ofstream ENERGY_RK4file;
	ENERGY_RK4file.open(ss.str().c_str());

	double k1, l1, k2, l2, k3, l3, k4, l4;
	for (int n = 0; n <= 100000; ++n)
	{
		k1 = deltatau*f(tau(n, deltatau), X.at(n), dX.at(n));
		l1 = deltatau*g(tau(n, deltatau), X.at(n), dX.at(n), Q);
		k2 = deltatau*f((tau(n, deltatau) + 0.5*deltatau), (X.at(n) + 0.5*k1), (dX.at(n) + 0.5*l1));
		l2 = deltatau*g((tau(n, deltatau) + 0.5*deltatau), (X.at(n) + 0.5*k1), (dX.at(n) + 0.5*l1), Q);
		k3 = deltatau*f((tau(n, deltatau) + 0.5*deltatau), (X.at(n) + 0.5*k2), (dX.at(n) + 0.5*l2));
		l3 = deltatau*g((tau(n, deltatau) + 0.5*deltatau), (X.at(n) + 0.5*k2), (dX.at(n) + 0.5*l2), Q);
		k4 = deltatau*f((tau(n, deltatau) + deltatau), (X.at(n) + k3), (dX.at(n) + l3));
		l4 = deltatau*g((tau(n, deltatau) + deltatau), (X.at(n) + k3), (dX.at(n) + l3), Q);
		X.push_back((X.at(n) + (k1 / 6) + (k2 / 3) + (k3 / 3) + (k4 / 6)));
		dX.push_back((dX.at(n) + (l1 / 6) + (l2 / 3) + (l3 / 3) + (l4 / 6)));
		x.push_back(exp(-(PI * tau(n, deltatau)) / (2 * Q)) * (cos(tau(n, deltatau) * (2 * PI) * (sqrt(1 - (1 / (4 * pow(Q, 2))))))));

		ENERGY_RK4file << n << "\t" << (pow(X.at(n), 2) + pow(dX.at(n), 2)) << endl;

	}
	ENERGY_RK4file.close();
}



